TP Boucle (exercice-loop.html / exercice-loop.js)
Faire deviner un nombre en 1 et 10 à l'utilisateur.ice en utilisant un prompt, une boucle et une/des conditions
1) Créer un fichier exercice-loop.html et un js/exercice-loop.js (et charger le script et tout)
2) Commencer par réussir à récupérer les entrées utilisateur.ice avec un prompt, et afficher la valeur du prompt en console
3) Puis utiliser une boucle pour faire en sorte qu'on re-prompt quelque chose à l'utilisateur.ice à jamais (donc une boucle qui contient un prompt), et toujours afficher la valeur utilisateur.ice en console
4) Ensuite, faire une condition qui affichera en console "bravo" si jamais l'entrée utilisateur.ice correspond à '5' par exemple
5) Maintenant, faire en sorte que dans cette condition, on change la variable du while pour stopper la boucle
Bonus : 
1) Faire que la boucle nous dise si le nombre à deviner et supérieur ou inférieur à celui qu'on vient d'entrée
2) Faire que le nombre à deviner soit sélectionné de manière random
3) Faire qu'on puisse choisir le niveau de difficulté au début (entre 1 et 10, entre 1 et 100, entre 1 et 1000)



TP  Boucle Pyramide (exercice-pyramid.html / js/exercice-pyramid.js)
Faire une pyramide d'étoile de 5 étages en console en utilisant des boucles.(modifié)
1) Créer un fichier exercice-pyramid.html et exercice-pyramid.js (faire le lien entre les deux et tout)
2) Commencer par faire une première boucle pour avoir 5 lignes en consoles (attention, quand on console log plusieurs fois de suite la même chose, le navigateur va juste mettre une ligne avec le nombre de fois qu'elle est répétée, mais c'est ok)
3) Faire ensuite une deuxième boucle à l'intérieur de la première afin d'obtenir un carré/rectangle d'étoile. La première boucle servira à faire les lignes, et la boucle à l'intérieur servira à faire le nombre d'étoile voulu par ligne
4) Une fois qu'on a notre "carré/rectangle", on tente de trouver un calcul qui permettra de faire que la première ligne ait 1 étoile, puis 3, puis 5 etc. (ou tout autre nombre, tant que ça fait une pyramide à la fin). Le but est cette fois d'obtenir un triangle rectangle calé sur la gauche de la console
5) Trouver un calcul pour obtenir le nombre d'espaces qu'il faudra avant chaque rangée d'étoiles, puis faire une nouvelle boucle (qui sera au même niveau que la boucle d'étoile, mais juste avant) pour mettre ces espaces (pensez à ce qu'on a fait hier avec la declaration d'amour, il va falloir avoir une variable à laquelle on ajoutera au fur et à mesure des " " et des "*"  avec un += )
Bonus : Faire en sorte de pouvoir choisir le nombre d'étage de la pyramide (soit via une variable, soit via un prompt)