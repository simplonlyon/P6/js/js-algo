//On fait une variable témoin qui dira si on continue le jeu ou pas
let game = true;
/*
On fait un premier prompt pour demander à l'utilisateur.ice le
nombre maximal possible.
On utilise la fonction parseInt afin de convertir la réponse de 
string vers number.
*/
let max = parseInt( prompt('Between 0 and which number do you want ?') );
//On utilise les fonctions mathématiques de javascript pour choisir
//un nombre aléatoire à deviner. (voir la doc pour ces fonctions)
//On utilise le nombre max choisi juste avant pour obtenir ce nombre
let answer = Math.round(Math.random()*max);
console.log(answer);
//On fait une boucle, tant que game est true, on fait la boucle
while (game) {
    //On demande à l'utilisateur.ice un nombre, qu'on converti en
    //number avec le parseInt
    let input = parseInt( prompt("Guess a number ?") );

    //Si l'input est inférieur à la réponse, on affiche un message
    if(input < answer) {
        console.log('it\'s more');
    }
    //Si l'input est supérieur à la réponse, on affiche un message
    if(input > answer) {
        console.log('it\'s less');
    }
    //Si l'input est la réponse...
    if(input === answer) {
        //...on dit bravo...
        console.log('congrats');
        //...et on passe la variable game à false pour stoper la boucle.
        game = false;
    }

}
//On met un petit message de fin de jeu
console.log('end game');


