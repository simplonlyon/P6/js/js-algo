/*
Un tableau ou array est une suite de données stockée dans un
même emplacement (plus ou moins)
Les tableaux peuvent contenir n'importe quel type de données (même
des tableaux de tableaux si on veut)
Chaque donnée du tableau sera assignée à un index de celui ci.
Les index sont une valeur numérique qui commencent à zéro.
Ici, le tableau myArray a cette correspondance index/valeur
index   0    |   1   |   2   |   3   
valeur  "ga" |  "zo" |  "bu" | "meu"
*/
let myArray = ["ga", "zo", "bu", "meu"];
//Pour accéder à une valeur du tableau, on écrit le nom de
//la variable qui contient le tableau, puis entre crochets l'index
//de la valeur à laquelle on veut accéder
console.log( myArray[1] ); //affiche zo en console

/*
De la même manière qu'il est fortement déconseillé de changer
le type d'une variable au cours de sa vie, il ne vaut mieux
pas faire  de tableau contenant des valeurs de types différents
*/
//let myArrayPourry = [1, true, "ga", undefined];


let evenNumber = [2, 4, 6, 8, 10];

console.log(evenNumber[2]); //affiche 6

evenNumber[5] = 12;
//Array.push rajoute un élément au bout du tableau
evenNumber.push(14);
//Array.splice supprime un ou plusieurs élément d'un tableau
evenNumber.splice(1, 1);

console.log(evenNumber[2]);//affiche 8

/*
Pour parcourir les valeurs d'un tableau, on peut utiliser un 
for classique en lui donnant comme valeur d'arrêt la taille du
tableau à parcourir
*/
for(let x = 0; x < myArray.length; x++) {
    //On utilise ensuite x pour accéder aux valeurs
    console.log(myArray[x]);
}
//Ou bien utiliser le for...of qui parcours automatiquement les
//valeurs du tableau à droite du of et les place dans la variable
//déclarée à gauche du of
for(let valeur of myArray) {
    //Ici, les valeurs sont disponible dans la variable valeur
    //mais on aurait pu l'appeler comme on veut
    console.log(valeur);
}