//On dit dans cette variable combien de ligne fera la pyramide
let lineNumber = 14;
//On calcul le nombre d'étoile maximal en se basant sur le nombre de ligne
let maxStar = lineNumber * 2;
//On lance une première boucle en se basant sur le nombre de ligne
for(let x = 1; x <= lineNumber; x++) { //boucle du nombre d'étages
    //On calcul le nombre d'étoile pour la ligne actuelle (x)
    //Le but est d'avoir un nombre d'étoiles impair pour faire une
    //jolie pyramide
    let starNumber = x*2 - 1;
    //On calcul ensuite le nombre d'espace en utilisant le nombre
    //d'étoiles maximum et le nombre d'étoiles actuel calculés au dessus
    let spaceNumber = (maxStar-starNumber) / 2 ;
    //On initialise une variable line qui contiendra ce qu'on affichera
    //sur la ligne actuelle
    let line = '';
    //On lance une deuxième boucle pour générer le nombre d'espace voulu
    for(let y = 1; y <= spaceNumber; y++) { //boucle du nombre d'espace
        //On ajoute à la variable ligne le caractère voulu pour l'espace
        line += ' ';
    }
    //On lance une troisième boucle pour générer le nombre d'étoiles voulu
    for(let y = 1; y <= starNumber; y++) { //boucle du nombre d'étoile
        //On ajoute à la variable ligne le caractère voulu pour l'étoile    
        line += '*';
    }
    //On affiche la ligne actuelle
    console.log(line);
}


// for(let x = 1; x <= lineNumber; x++) {
//     let starNumber = x*2 - 1;
//     let spaceNumber = (maxStar-starNumber) / 2 ;
//     console.log(' '.repeat(spaceNumber) + '*'.repeat(starNumber));
// }