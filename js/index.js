/*
Une variable en prog est une boîte nommée dans
laquelle on va stocker des choses pour les
réutiliser plus tard dans le code.
Pour déclarer une variable en JS, on utilise le mot clef let
(ou const pour déclarer une constante qui ne pourra pas être modifiée
après initialisation)
*/
let first = 1;
//Une fois déclarée, on peut changer la valeur de la variable sans
//remettre le mot clef let
first = 3;

/*
Le Js étant typé dynamiquement, le type d'une variable peut changer
au cours de sa vie. Ce n'est ceci dit vraiment pas conseillé de
changer le type d'une variable sous peine de bugs.
*/

/*
Les types primitifs existant en Javascript sont :
number (pour toutes les valeurs numériques)
string (pour les chaînes de caractères, soit entre guillemets ou entre quotes)
boolean (pour true ou false)
undefined (pour les variable qui n'existent pas)
null (pour la valeur null, qu'est un peu particulière)

toutes les autres valeurs seront de type object
*/
// first = "bloup";
// first = 'bloup';

// first = true;

// first = undefined;
//on peut utiliser console.log() pour afficher une valeur en console
//dans le navigateur
console.log(first);

/* 
    Convention de nommage
    En JS, les noms de variable doivent être ecrit en camelCase, 
    c'est à dire tout attaché, sans caractère spéciaux, sans accents,
    sans tirets, avec la première lettre en minuscule puis la première
    lettre de chaque nouveau mot en majuscule.
    (maVariable et mavariable sont deux variables différentes)
*/
let unExempleDeNomDeVariable;
