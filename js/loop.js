/*
La boucle for permet de répéter une instruction selon une condition
On déclare en premier une variable compteur, en l'initialisant à
la valeur voulue.
Puis on indique la condition qui fera continuer la boucle tant 
qu'elle est vraie.
Puis on indique la modification à appliquer à la variable compteur à
la fin de chaque tour de boucle.
Ici, on initialise une variable x à 0, la boucle continuera tant que
x est inférieur à 5, et à chaque tour de boucle, on rajoute 1 à la
valeur de x.
*/
for(let x = 0; x < 5; x++) {
    console.log(x);
}

/*
Ici, je fais une boucle pour dire à mon chien que je l'aime, en
faisant répéter le "very" par une boucle.
Pour que la phrase finale soit affichée en une seule fois, on va
utiliser une variable dont on changera la valeur à chaque tour de boucle

*/
//on commence par créer la variable avec le début de la phrase
let loveLetter = "Pupper, I love you";
//puis je lance la boucle, qui va répéter combien de very je veux
for(let x = 0; x < 5; x++) {
    //à chaque tour de boucle, on modifie la variable pour rajouter
    //un " very " à la fin de celle ci
    loveLetter += " very ";
    //la notation du dessus est équivalente à :
    //loveLetter = loveLetter + " very ";
}
//Une fois la boucle terminée, je rajoute la fin de ma phrase à la variable
loveLetter += "much.";
//J'affiche la variable qui contient la phrase complète dans la console
console.log(loveLetter);

let go = true;
//Le while tourne tant que la valeur entre ses parenthèse est true
while(go) {
    console.log("in my while");
    //si je change la valeur de go à false, forcément le while s'arrêtera à la fin du premier tour
    go = false;
}

//refaire la première boucle que j'ai fait en for, mais avec un while

let counter = 0;
while(counter < 5) {
    console.log(counter);
    counter++;
}

/*
for(let x = 0; x < 5; x++) {
    if(x === 2) {
        //Le mot clef continue permet d'arrêter l'exécution du
        //tour de boucle actuel pour passer au tour de boucle
        //suivant. Dans cet exemple, la valeur 2 ne sera pas inscrite
        //en console
        continue;
    }
    console.log(x);
}

while(true) {
    let input = prompt('guess');

    if(input === '5') {
        //Le mot clef break permet d'arrêter l'exécution d'une
        //boucle. Si l'input vaut '5', le while s'arrête.
        break;
    }
    
}
*/